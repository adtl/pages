---
title: 欢迎使用ADTL,Api测试工具
date: 2019-05-05 18:00:00
tags:
id: 1
---

   ADTl,Api测试工具,PostMan的替代品.

## 特色

* 开源,免费,可修改
* 无服务器依赖
* 结构化数据
* 版本控制
* Mock数据生成

## 储存内容格式如下:

https://gitee.com/adtl/storage_demonstration



## 获取方式:

1. 下载,到版本发布页面下载已打包的可执行文件  [ 发行版 ](https://gitee.com/adtl/adtl/releases)

2. [ Snap 仓库](https://snapcraft.io/adtl)

3. 自打包,下载源码进行打包 [ 源代码 ](https://gitee.com/adtl/adtl)