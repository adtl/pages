---
title: Snap踩坑指南
date: 2019-05-06 18:00:00
tags:
id: 2
---

   ADTL发布了Snap版本,但是这个家伙吧坑不少,下面是踩坑指南:

## 我的系统

![图片](/pages/images/2019-05-0616-50-00.png)



## snap 无法读取家目录(home/用户目录)之外的文件,也就是无法在用户目录之外建立项目

* snap运行需要允许访问家目录的文件,播放和录制声音权限不必须(目前没有任何声音提示和录制功能)


![图片](/pages/images/snap_017.png)





### 解决：Gtk-Message: Failed to load module "canberra-gtk-module"

执行下面的命令：
```
   sudo apt-get install libcanberra-gtk-module 
```


